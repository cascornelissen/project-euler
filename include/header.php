<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Project Euler</title>
	<link href='//fonts.googleapis.com/css?family=Alegreya+Sans:400,700|Source+Code+Pro:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/prism.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="js/prism.min.js"></script>
	<script src="js/main.js"></script>
</head>
<body>
    <header>
        <div class="wrapper">
            <h1>
                <?php
                    echo ( !$logo ) ? 'Project Euler' : '<a href="./" title="Back to overview">Project Euler</a>';
                ?>
            </h1>
            <small>Solved problems by Cas Cornelissen</small>
        </div>
    </header>
    <main>
        <div class="wrapper">