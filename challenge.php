<?php
    $logo = true;
    require("include/header.php");
?>
<?php
    if ( isset($_GET["id"]) ) {
        include('challenges/' . $_GET["id"] . '.php');
    } else {
        ?>
        <h2>Error</h2>
        <p>No challenge identifier was specified. Go back to the <a href="./">overview</a>.</p>
        <?php
    }
?>
<?php require("include/footer.php") ?>