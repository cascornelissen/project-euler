<h2>5. Smallest multiple</h2>
<span class="badge solved">Solved</span>

<h3>Description</h3>
<p>2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.</p>
<p>What is the smallest positive number that is <dfn title="divisible with no remainder">evenly divisible</dfn> by all of the numbers from 1 to 20?</p>

<h3>Code</h3>
<pre>
<code class="language-php">$i = 0;
$max = 20;

while(1) {
    $i += $max;
    
    for ( $j = 2; $j <= $max; $j++ ) {
        if ( $i % $j != 0 ) break;
        if ( $j == $max ) {
            echo $i; // Answer: 232792560
            exit;
        }
    }
}</code>
</pre>

<h3>Answer</h3>
<p>232792560</p>