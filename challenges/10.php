<h2>10. Summation of primes</h2>
<span class="badge solved">Solved</span>

<h3>Description</h3>
<p>The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.</p>
<p>Find the sum of all the primes below two million.</p>

<h3>Code</h3>
<pre>
<code class="language-php">// Optimized using the Sieve of Eratosthenes

$p = 2;
$n = 2000000;
$range = range($p, $n);
$primes = array_combine($range, $range);

while ( $p * $p < $n ) {
  for ( $i = $p; $i <= $n; $i += $p ) {
      if ($i == $p) continue;
      unset($primes[$i]);
    }
    
    $p = next($primes);
}

echo array_sum($primes); // Answer: 142913828922</code>
</pre>

<h3>Answer</h3>
<p>142913828922</p>