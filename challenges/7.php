<h2>7. 10001st prime</h2>
<span class="badge solved">Solved</span>

<h3>Description</h3>
<p>By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.</p>
<p>What is the 10 001st prime number?</p>

<h3>Code</h3>
<pre>
<code class="language-php">$i = 1;
$prime = 1;

while($prime < 10001) {
    $i += 2;
    if ( isprime($i) ) $prime++;
}

echo $i; // Answer: 104743

function isprime($n) {
    if ( $n == 1 ) return false;
    if ( $n == 2 ) return true;
    if ( $n%2 == 0 ) return false;
  
    for ( $i = 3; $i <= ceil(bcsqrt($n)); $i += 2 ) {
        if ( bcmod($n, $i) == 0 ) return false;
    }
    
    return true;
}</code>
</pre>

<h3>Answer</h3>
<p>104743</p>