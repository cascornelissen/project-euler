<h2>14. Longest Collatz sequence</h2>
<span class="badge workingon">Working on</span>

<h3>Description</h3>
<p>The following iterative sequence is defined for the set of positive integers:</p>
<pre>
n → n/2 (n is even)
n → 3n + 1 (n is odd)
</pre>
<p>Using the rule above and starting with 13, we generate the following sequence:</p>
<p>13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1</p>
<p>It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.</p>
<p>Which starting number, under one million, produces the longest chain?</p>
<p><strong>NOTE:</strong> Once the chain starts the terms are allowed to go above one million.</p>

<h3>Code</h3>
<pre>
<code class="language-php">__CODE__</code>
</pre>

<h3>Answer</h3>
<p>__ANSWER__</p>

<?php

$n = 50;
$cache = array();

function collatz($x) {
	return ($x % 2 == 0) ? ($x / 2) : ($x * 3 + 1);
}

for ( $i = 0; $i < $n; $i++ ) {
	
}

?>