<h2>1. Multiples of 3 and 5</h2>
<span class="badge solved">Solved</span>

<h3>Description</h3>
<p>If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.</p>
<p>Find the sum of all the multiples of 3 or 5 below 1000.</p>

<h3>Code</h3>
<pre>
<code class="language-php">$j = 0;

for ( $i = 1; $i < 1000; $i++ ) {
    if ( $i%3 == 0 || $i % 5 == 0 ) $j += $i;
}

echo $j; // Answer: 233168</code>
</pre>

<h3>Answer</h3>
<p>233168</p>