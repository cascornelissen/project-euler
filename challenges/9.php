<h2>9. Special Pythagorean triplet</h2>
<span class="badge solved">Solved</span>

<h3>Description</h3>
<p>A Pythagorean triplet is a set of three natural numbers, <var>a</var> < <var>b</var> < <var>c</var>, for which, <var>a</var><sup>2</sup> + <var>b</var><sup>2</sup> = <var>c</var><sup>2</sup></p>
<p>For example, 3<sup>2</sup> + 4<sup>2</sup> = 9 + 16 = 25 = 5<sup>2</sup>.</p>
<p>There exists exactly one Pythagorean triplet for which <var>a</var> + <var>b</var> + <var>c</var> = 1000.<br>Find the product <var>abc</var>.</p>

<h3>Code</h3>
<pre>
<code class="language-php">$s = 1000;

for ( $a = 1; $a < $s; $a++ ) {
    for ( $b = $a; $b < $s; $b++ ) {
        $c = $s - $a - $b;
        
        if ( ($a * $a) + ($b * $b) == ($c * $c) ) {
            echo $a * $b * $c; // Answer: 31875000
            break;
        }
    }
}</code>
</pre>

<h3>Answer</h3>
<p>31875000</p>