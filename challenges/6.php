<h2>6. Sum square difference</h2>
<span class="badge solved">Solved</span>

<h3>Description</h3>
<p>The sum of the squares of the first ten natural numbers is: 1<sup>2</sup> + 2<sup>2</sup> + ... + 10<sup>2</sup> = 385</p>
<p>The square of the sum of the first ten natural numbers is: (1 + 2 + ... + 10)<sup>2</sup> = 55<sup>2</sup> = 3025</p>
<p>Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.</p>
<p>Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.</p>

<h3>Code</h3>
<pre>
<code class="language-php">$sumofsquares = 0;
for ( $i = 1; $i <= 100; $i++ ) {
    $sumofsquares += pow($i, 2);
}

$squareofsums = 0;
for ( $i = 1; $i <= 100; $i++ ) {
    $squareofsums += $i;
}

echo pow($squareofsums, 2)-$sumofsquares; // Answer: 25164150</code>
</pre>

<h3>Answer</h3>
<p>25164150</p>