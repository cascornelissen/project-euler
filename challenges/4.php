<h2>4. Largest palindrome product</h2>
<span class="badge solved">Solved</span>

<h3>Description</h3>
<p>A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 &times; 99.</p>
<p>Find the largest palindrome made from the product of two 3-digit numbers.</p>

<h3>Code</h3>
<pre>
<code class="language-php">$palindromes = array();

for ( $i = 1000; $i > 100; $i-- ) {
    for ( $j = 1000; $j > 100; $j-- ) {
        $x = $i * $j;
        if ( $x == strrev($x) ) $palindromes[] = $x;
    }
}

echo max($palindromes); // Answer: 906609</code>
</pre>

<h3>Answer</h3>
<p>906609</p>