<h2>3. Largest prime factor</h2>
<span class="badge solved">Solved</span>

<h3>Description</h3>
<p>The prime factors of 13195 are 5, 7, 13 and 29.</p>
<p>What is the largest prime factor of the number 600851475143?</p>

<h3>Code</h3>
<pre>
<code class="language-php">$x = 600851475143;
$sqrt = floor(sqrt($x));
$factors = array();

for ( $i = $sqrt; $i > 0; $i-- ) {
    $remainder = fmod($x, $i);
    if ( $remainder == 0 && isprime($i) ) { 
        echo $i; // Answer: 6857
        break;
    }
}

function isprime($n) {
    if ( $n == 1 ) return false;
    if ( $n == 2 ) return true;
    if ( $n%2 == 0 ) return false;
  
    for ( $i = 3; $i <= ceil(bcsqrt($n)); $i += 2 ) {
        if ( bcmod($n, $i) == 0 ) return false;
    }
    
    return true;
}</code>
</pre>

<h3>Answer</h3>
<p>6857</p>