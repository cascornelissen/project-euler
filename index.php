<?php
    $logo = false;
    require("include/header.php");
?>
<h3>What is Project Euler?</h3>
<p>Project Euler is a series of challenging mathematical/computer programming problems that will require more than just mathematical insights to solve. Although mathematics will help you arrive at elegant and efficient methods, the use of a computer and programming skills will be required to solve most problems.</p>

<h3>The projects I've been working on</h3>
<table>
    <thead>
        <tr>
            <td>ID</td>
            <td>Description / Title</td>
            <td>Status</td>
        </tr>
    </thead>
    <tbody>
        <?php
            $challenges = glob("challenges/*[0-9].php");
            natsort($challenges);
            
            foreach ( $challenges as $i => $challenge ) {
                $lines = file($challenge, FILE_IGNORE_NEW_LINES);
                $details = explode(".", strip_tags($lines[0]));
                
                ?>
                <tr> 
                    <td><?= $details[0] ?></td>
                    <td><a href="./challenge?id=<?= $details[0] ?>"><?= $details[1] ?></a></td>
                    <td><?= strip_tags($lines[1]) ?></td>
                </tr>
                <?php
            }
        ?>
    </tbody>
</table>
<?php require("include/footer.php") ?>